# Ultimate-Install-Script
This script is made for Ubuntu Based distrubitons only!! 
To install this Script:
1. Download or Clone my github repo
2. Make sure the file has execution rights
3. Open a terminal inside the directory where you downloaded or cloned my repo
4. Type ./UltimateInstall.sh





This Script Installs the following software:
1. Gimp
2. Spotify
3. Neofetch
4. Redshift
5. Inkscape
6. Audacity
7. Handbrake
8. Virtualbox
9. Simple Screen Recorder
10. OBS
11. Make MKV
12. Ice
13. Updated PPA for Kdenlive
14. Chromium
15. Timeshift
16. Grsync
18. VeraCrypt



This Script Also installs the following themes:
1. Numix
2. Adapta
3. Flatabulous
4. LUV
5. Flat Remix
6. Arc
7. Obsidian
8. Papruis
9. Breeze Cursors

If you would Like some nice wallpapers Visit my website, I have provided a zip folder full of about 100
http://www.dreamincode.ca

 