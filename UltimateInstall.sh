sudo echo "Welcome First We Will Install Updates and Dependencies"
echo "Installing Dependencies"
sudo apt install -y git gdebi unzip ffmpeg snapd
echo "INSTALLING UPDATES"
sudo apt update
sudo apt dist-upgrade -yy
echo "STARTING NOW"
#START
mkdir ~/bin
cd ~/bin
#Download Packages
wget https://launchpad.net/~peppermintos/+archive/ubuntu/p6-release/+files/ice_5.0.1_all.deb
wget https://github.com/geigi/cozy/releases/download/0.4.0/com.github.geigi.cozy_0.4.0_amd64.deb
#Install Packages
sudo gdebi ice_5.0.1_all.deb
sudo gdebi com.github.geigi.cozy_0.4.0_amd64.deb
#Remove DEB Files
sudo rm ice_5.0.1_all.deb
sudo rm com.github.geigi.cozy_0.4.0_amd64.deb
#Install Additonal Software
sudo apt install -y gimp inkscape audacity handbrake virtualbox virtualbox-qt virtualbox-ext-pack gnome-disk-utility filezilla calibre plank grsync soundconverter
#Insync
#sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C
#sudo apt update
#sudo apt install -y insync
#Timeshift
sudo add-apt-repository -y ppa:teejee2008/ppa
sudo apt update
sudo apt install -y timeshift
#Make Mkv
sudo add-apt-repository ppa:heyarje/makemkv-beta -y
sudo apt-get update
sudo apt install -y makemkv-bin
#Simple Screen Recorder
sudo add-apt-repository ppa:maarten-baert/simplescreenrecorder -y
sudo apt-get update
sudo apt install -y simplescreenrecorder
#OBS
sudo add-apt-repository ppa:obsproject/obs-studio -y
sudo apt update
sudo apt install -y obs-studio
#Spotify
sudo snap install spotify
#Kdenlive
sudo add-apt-repository ppa:kdenlive/kdenlive-stable -y
sudo apt-get update
sudo apt-get install -y kdenlive
#VeraCrypt
wget https://launchpadlibrarian.net/289850375/veracrypt-1.19-setup.tar.bz2
mkdir veracrypt
tar xjvf veracrypt-1.19-setup.tar.bz2 -C veracrypt
cd veracrypt
./veracrypt-1.19-setup-gui-x64
#Neofetch
sudo add-apt-repository ppa:dawidd0811/neofetch -y
sudo apt-get update
sudo apt install -y neofetch
echo "neofetch" >> ~/.bashrc	#Append ~/.bashrc for neofetch

#Adjust Screen Tempture
sudo apt install -y redshift redshift-gtk




#####THEMES########################################################################################################################
#Breeze Cursor
sudo apt-get install -y breeze-cursor-theme
#Papruis
sudo add-apt-repository ppa:papirus/papirus -y
sudo apt update
sudo apt install -y papirus-icon-theme
#Numix
sudo add-apt-repository ppa:numix/ppa -y
sudo apt-get update
sudo apt-get install -y numix-gtk-theme numix-icon-theme-circle
#Paper
sudo add-apt-repository ppa:snwh/pulp -y
sudo apt-get update
sudo apt-get install -y paper-icon-theme
sudo apt-get install -y paper-cursor-theme
sudo apt-get install -y paper-gtk-theme
#Flatabulous
sudo add-apt-repository ppa:noobslab/icons -y
sudo apt-get update
sudo apt-get install -y ultra-flat-icons ultra-flat-icons-green ultra-flat-icons-orange
#LUV
git clone https://github.com/Nitrux/luv-icon-theme.git
cd luv-icon-theme/
sudo cp -r Luv/ /usr/share/icons
cd ..
sudo rm -r luv-icon-theme/
#Flat Remix
git clone https://github.com/daniruiz/Flat-Remix.git
cd Flat-Remix/
sudo cp -r Flat\ Remix/ /usr/share/icons
sudo cp -r Flat\ Remix\ Dark/ /usr/share/icons
sudo cp -r Flat\ Remix\ Light/ /usr/share/icons
cd ..
sudo rm -r Flat-Remix/
#Arc
sudo add-apt-repository ppa:noobslab/themes -y
sudo apt-get update
sudo apt-get install -y arc-theme
#Obsidian
sudo add-apt-repository ppa:noobslab/themes -y
sudo apt-get update
sudo apt-get install -y obsidian-gtk-theme
